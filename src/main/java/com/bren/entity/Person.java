package com.bren.entity;

abstract class Person {
    private String name;
    private String address;
    private int age;

    Person(String name, String address, int age) {
        this.name = name;
        this.address = address;
        this.age = age;
    }

    abstract String getInfo();

    String getName() {
        return name;
    }

    String getAddress() {
        return address;
    }

    int getAge() {
        return age;
    }

}
