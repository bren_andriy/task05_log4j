package com.bren.entity;

public class Student extends Person {
    private String university;
    private String faculty;
    private int yearOfEducation;

    public Student(String name, String address, int age, String university, String faculty, int yearOfEducation) {
        super(name, address, age);
        this.university = university;
        this.faculty = faculty;
        this.yearOfEducation = yearOfEducation;
    }

    @Override
    public String getInfo() {
        return  "====Info=====\n"
                + "---Student---\n"
                + "Name: " + getName() + "\n"
                + "Address: " + getAddress() + "\n"
                + "Age: " + getAge() + "\n"
                + "University: " + university + "\n"
                + "Faculty: " + faculty + "\n"
                + "Year of education: " + yearOfEducation + "\n";
    }
}
