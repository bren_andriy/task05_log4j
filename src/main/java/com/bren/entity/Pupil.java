package com.bren.entity;

public class Pupil extends Person {
    private int schoolId;

    public Pupil(String name, String address, int age, int schoolId) {
        super(name, address, age);
        this.schoolId = schoolId;
    }

    @Override
    public String getInfo() {
        return "====Info=====\n"
                + "---Pupil---\n"
                + "Name: " + getName() + "\n"
                + "Address: " + getAddress() + "\n"
                + "Age: " + getAge() + "\n"
                + "School № " + schoolId + "\n";
    }
}
