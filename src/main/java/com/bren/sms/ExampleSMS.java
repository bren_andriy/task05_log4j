package com.bren.sms;

import com.twilio.Twilio;
import com.twilio.rest.api.v2010.account.Message;
import com.twilio.type.PhoneNumber;

public class ExampleSMS {
    // Find your Account Sid and Token at twilio.com/user/account
    public static final String ACCOUNT_SID = "ACafbbd57d9660c2247a8065c8137e0bff";
    public static final String AUTH_TOKEN = "23cdd59823b381bebb233aa71ebe3ff3";

    public static void send(String str) {
        Twilio.init(ACCOUNT_SID, AUTH_TOKEN);
        Message message = Message
                .creator(new PhoneNumber("+380937131779"),
                        new PhoneNumber("+14122285756"),str).create();
    }
}

