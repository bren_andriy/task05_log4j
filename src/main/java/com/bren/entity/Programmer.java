package com.bren.entity;

public class Programmer extends Person {
    private String profession;

    public Programmer(String name, String address, int age, String profession) {
        super(name, address, age);
        this.profession = profession;
    }

    @Override
    public String getInfo() {
        return  "====Info=====\n"
                + "---Programmer---\n"
                + "Name: " + getName() + "\n"
                + "Address: " + getAddress() + "\n"
                + "Age: " + getAge() + "\n"
                + "Profession: " + profession + "\n";
    }
}
