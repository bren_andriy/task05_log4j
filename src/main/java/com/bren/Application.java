package com.bren;

import com.bren.entity.Programmer;
import com.bren.entity.Pupil;
import com.bren.entity.Student;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;


public class Application {
    private static Logger logger = LogManager.getLogger(Application.class);
    public static void main(String[] args) {

        Programmer programmer = new Programmer("Andrii","Ukraine, Lviv",20,"Front-End");
        Programmer programmer1 = new Programmer("Roksa","Lutsk",25,"Back-End");
        Student student = new Student("Ostap","Ukraine, Kyiv",19,
                "KP","Computer Science",3);
        Pupil pupil = new Pupil("Ivan","Lviv",12,73);
        Pupil pupil1 = new Pupil("Ira", "Odessa",15,28);
        System.out.println(programmer.getInfo());
        System.out.println(programmer1.getInfo());
        System.out.println(student.getInfo());
        System.out.println(pupil.getInfo());
        System.out.println(pupil1.getInfo());

        logger.error("Exception", new IOException("Hello, test"));
        logger.trace("This is a trace message");
        logger.debug("This is a debug message");
        logger.info("This is a info message");
        logger.warn("This is a warn message");
        logger.error("This is a error message");
        logger.fatal("This is a fatal message");


    }
}
